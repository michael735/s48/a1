

export default function Footer () {
    return(
        <div className="bg-info text-white d-flex justify-content-center fixed-bottom align-items-center" style={{height: '10vh', marginTop: '50px'}}>
            <p className="m-0 font-weight-bold">Michael &#64; Course  Booking | Zuitt Coding Bootcamp &#169;</p>     
        </div>
    )
}