import {Card, Button} from 'react-bootstrap';
import { useState } from 'react';
export default function CourseCard({courseProp}) {

    let [count, setCount] = useState(1)
    let [seats, setSeats] = useState(30)

    const {name, description, price} = courseProp

    const handleClick = () =>{
        //console.log(`Clicked`, count+1)
        if(seats > 1){

            setCount(count + 1)
            setSeats(seats-1)

        }else{    

            setSeats(alert(`No more seats`));
            setSeats(`No more seats`)
        }
    }

    return(
                    <Card className="m-5">
                        <Card.Body>
                            <Card.Title className='font-weight-bold'>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            <Card.Subtitle>Enrolees:</Card.Subtitle>
                            <Card.Text>Enrollees: {count}</Card.Text>
                            <Card.Text>Seats Available: {seats}</Card.Text>
                            <Button className="btn-info" onClick={handleClick}>Enroll</Button>
                        </Card.Body>
                    </Card>
    )

}