import {Card,Container, Col, Row} from 'react-bootstrap';

export default function Highlights() {
    return (
        <Container>
            <Row>
                <Col xs={12} md={4}>
                    <Card >
                        <Card.Body>
                            <Card.Title>
                                <h3 className='display-5 font-weight-bold'>Learn from Home</h3>
                            </Card.Title>
                            <Card.Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt alias ad velit iusto. Repellat quis expedita magni consequatur esse velit eius quibusdam, iste inventore porro. Fugit temporibus beatae nulla atque?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card >
                        <Card.Body>
                            <Card.Title>
                                <h3 className='display-5 font-weight-bold'>Study Now, Pay Later</h3>
                            </Card.Title>
                            <Card.Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt alias ad velit iusto. Repellat quis expedita magni consequatur esse velit eius quibusdam, iste inventore porro. Fugit temporibus beatae nulla atque?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card >
                        <Card.Body>
                            <Card.Title>
                                <h3 className='display-5 font-weight-bold'>Be Part of Our Community</h3>
                            </Card.Title>
                            <Card.Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt alias ad velit iusto. Repellat quis expedita magni consequatur esse velit eius quibusdam, iste inventore porro. Fugit temporibus beatae nulla atque?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>



    )
}