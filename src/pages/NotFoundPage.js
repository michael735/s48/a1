
import { Link } from "react-router-dom";
import Banner from "../components/Banner";
import { Fragment } from "react";
import bannerData2 from "../mockData/banner2";


export default function NotFoundPage(){
    return(
        <Fragment>
            <Banner bannerProp={bannerData2}></Banner>
        </Fragment>
    )
}