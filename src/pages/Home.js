import { Fragment } from "react"
import Banner from "./../components/Banner"
import bannerData1 from "../mockData/banner1"
import Highlights from "../components/Highlights"

export default function Home(){
    return(
        <Fragment>
            <Banner bannerProp={bannerData1}/>
            <Highlights/>
        </Fragment>
    )
}